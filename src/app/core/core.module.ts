import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserModule} from '../user/user.module';
import {NewsModule} from '../news/news.module';
import {FriendsModule} from '../friends/friends.module';

@NgModule({
  imports: [
    NewsModule,
    UserModule,
    CommonModule,
    FriendsModule
  ],
  providers: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
  }
}
