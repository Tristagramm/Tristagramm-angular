import {Feed, Like} from './feed';
import {User} from '../user/user';

export class FeedClass implements Feed {
  created?: Date;
  likes?: Like[];
  objectId: string;
  owner: User;
  ownerId: string;
  photo?: string;
  text?: string;

  constructor(ownerId: string , owner: User, text?: string, photo?: string) {
    this.ownerId = ownerId;
    this.owner = owner;
    this.text = text;
    this.photo = photo;
    this.likes = <Like[]>[];
    this.created = new Date();
  }
}
