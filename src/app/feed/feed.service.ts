import {Injectable} from '@angular/core';
import {Feed, Like} from './feed';
import {DataProvider} from '../global/data-provider';
import {User} from '../user/user';

@Injectable()
export class FeedService extends DataProvider {

  private static readonly modelName = 'Feeds';
  private static readonly likesName = 'Likes';

  public getAllNews(): Promise<Feed[] | null> {
    const q = this.q(null).setRelated(['owner', 'likes']).setSortBy('created desc');
    return Backendless.Data.of(FeedService.modelName).find(q);
  }

  public getUsersNews(user: User): Promise<Feed[]> {
    const q = this.q('ownerId = ' + this.quot(user.objectId))
      .setRelated(['owner', 'likes']).setSortBy('created desc');
    return Backendless.Data.of(FeedService.modelName).find(q);
  }

  public getNewsOf(user: User): Promise<Feed[]> {
    const name = this.quot(user.name);
    const q = this.q('ownerId = ' + this.quot(user.objectId) +
      ' or ownerId in (Friends[friend1.name = ' + name + '].friend2.objectId)').setSortBy('created desc');
    return Backendless.Data.of(FeedService.modelName).find(q);
  }

  public getLikes() {
    return Backendless.Data.of(FeedService.likesName).find();
  }

  public addLike(): Promise<Like> {
    return Backendless.Data.of(FeedService.likesName).save(<Like>{});
  }

  public addLikeRelation(feed: Feed, like: Like) {
    return Backendless.Data.of(FeedService.modelName).addRelation(feed, 'likes', [like]);
  }

  public removeLike(like: Like) {
    return Backendless.Data.of(FeedService.likesName).remove(like).then();
  }

  public removeLikeRelation(feed: Feed, like: Like) {
    return Backendless.Data.of(FeedService.modelName).deleteRelation(feed, 'likes', [like]);
  }

  // public getLikesCount(feedId: string): Promise<number> {
  //   const query = 'feed = \'' + feedId + '\'';
  //   return Backendless.Data.of(FeedService.likesName).getObjectCount(this.q(query));
  // }

  public createFeed(feed: Feed) {
    const service = Backendless.Data.of(FeedService.modelName);
    return service.save(feed)
      .then(feedres => service
        .setRelation(feedres, 'owner', [feed.owner]));
  }

  public removeFeed(feed: Feed) {
    return Backendless.Data.of(FeedService.modelName).remove(feed);
  }

}
