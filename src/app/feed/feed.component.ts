import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FeedService} from './feed.service';
import {Feed, Like} from './feed';
import {UserService} from '../user/user.service';
import {USER_STORAGE} from '../routes/route';
import {DatePipe} from '@angular/common';
import {User} from '../user/user';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
  providers: [FeedService, DatePipe, UserService]
})
export class FeedComponent implements OnInit {
  @Input() feed: Feed;
  @Input() canBeDeleted?: boolean;
  @Output() deleted = new EventEmitter();
  likeClass: string;
  myLike?: Like;
  wait = false;

  constructor(private feedService: FeedService,private userService: UserService) {
  }

  ngOnInit() {
    this.checkLikes(this.feed.likes);
  }

  checkLikes(likes: Like[]) {
    for (const l of likes) {
      if (l.ownerId = UserService.currentUser.objectId) {
        this.likeClass = 'liked';
        this.myLike = l;
        return;
      }
    }
  }

  toggleLike() {
    if (this.wait) {
      return;
    }
    this.wait = true;
    if (this.myLike == null) {
      console.log('creation');
      this.feedService.addLike().then(e => {
        const like = <Like>e;
        console.log(like);
        this.feedService.addLikeRelation(this.feed, <Like> like)
          .then(() => this.setLike(like));
      });
    } else {
      console.log('removing');
      this.feedService.removeLike(this.myLike).then(() => this.removeLike());
    }
  }

  setLike(like: Like) {
    console.log('creation2: ' + like);
    this.likeClass = 'liked';
    this.myLike = like;
    this.feed.likes.push(like);
    this.wait = false;
  }

  removeLike() {
    console.log('removing2');
    this.likeClass = null;
    this.myLike = null;
    const idx = this.feed.likes.indexOf(this.myLike);
    this.feed.likes.splice(idx, 1);
    this.wait = false;
  }

  removeFeed() {
    this.feedService.removeFeed(this.feed).then(() => this.deleted.emit(this.feed));
  }

  getPhotoSourcePath() {
    return USER_STORAGE + this.feed.owner.name + '/' + this.feed.photo;
  }


  avatar(user: User) {
    return this.userService.getAvatarPath(user)
  }

}
