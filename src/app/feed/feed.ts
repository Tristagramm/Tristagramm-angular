import {User} from '../user/user';

export interface Feed {
  objectId?: string;
  owner?: User;
  text?: string;
  photo?: string;
  likes?: Like[];
  ownerId: string;
  created?: Date;
}

export interface Like {
  objectId: string;
  user?: User;
  ownerId: string;
  created: Date;
}
