import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FeedComponent} from './feed.component';
import {PhotoModule} from '../photo/photo.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    PhotoModule,
    RouterModule
  ],
  declarations: [
    FeedComponent
  ],
  exports: [FeedComponent]
})
export class FeedModule { }
