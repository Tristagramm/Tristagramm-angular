import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {RootComponent} from './root.component';
import {UserService} from './user/user.service';
import {GeolocationService} from './global/geolocation.service';
import {CoreModule} from './core/core.module';
import {RouterModule} from '@angular/router';
import {UserBarComponent} from './user/user-bar/user-bar.component';
import {PhotoModule} from './photo/photo.module';
import {FriendService} from './friends/friend.service';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    RootComponent,
    UserBarComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    CoreModule,
    RouterModule,
    PhotoModule,
    FormsModule
  ],
  providers: [UserService, GeolocationService, FriendService],
  bootstrap: [RootComponent]
})

export class AppModule {
}
