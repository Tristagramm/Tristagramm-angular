import {Page} from './routes/iroute';
import {Component, OnInit} from '@angular/core';
import {UserService} from './user/user.service';
import {Router} from '@angular/router';
import {User} from './user/user';
import {API_KEY, APPLICATION_ID} from './routes/route';
import {FriendService} from './friends/friend.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css'],
})
export class RootComponent implements OnInit {
  pages: Page[];
  linkedPage: Page[];
  user?: User;
  searchQuery?: string;
  subscribersCount?: number;
  subscribedCount?: number;

  constructor(private userService: UserService, private router: Router, private friendService: FriendService) {
    this.userService.userLogged$.subscribe(user => this.user = <User>user);
  }

  ngOnInit() {
    Backendless.initApp(APPLICATION_ID, API_KEY);
    if (this.user == null) {
      this.userService.currentUser().then(u => this.afterUserInit(u));
    }
  }

  afterUserInit(user: User) {
    this.userService.logInEvent(user);
    this.friendService.getSubscribersCount(user).then(s => this.subscribersCount = s);
    this.friendService.getSubscribedOnCount(user).then(s => this.subscribedCount = s);
  }

  onSearch() {
    this.router.navigate(['/search/' + this.searchQuery]).catch();
  }

}
