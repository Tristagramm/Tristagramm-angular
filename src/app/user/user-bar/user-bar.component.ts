import {Component, Input, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {USER_STORAGE} from '../../routes/route';

@Component({
  selector: 'app-user-bar',
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.css']
})
export class UserBarComponent implements OnInit {
  @Input() user?: User;
  @Input() opened: boolean;
  avatarPath: string;

  constructor(private userService: UserService, private router: Router) {
    userService.userLogged$.subscribe(user => this.updateUserStatus(<User>user));
  }

  ngOnInit() {
    if (this.user == null) {
      this.userService.currentUser().then(u => this.user = u);
    }
    if (this.user.avatar != null) {
      this.avatarPath = this.userService.getAvatarPath(this.user);
    }
  }

  logOut() {
    this.userService.logout().then(() => console.log('user logged out')).catch(e => console.log(e));
    this.router.navigate(['/']).then();
  }

  updateUserStatus(user: User) {
    this.user = user;
    this.ngOnInit();
  }
}
