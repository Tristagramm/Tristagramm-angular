import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../registration/registration.component.css']
})
export class LoginComponent implements OnInit {

  usrName?: string;
  password?: string;
  email?: string;
  restore: boolean;

  constructor(private userService: UserService, private router: Router) {
    this.restore = false;
  }

  ngOnInit() {
    this.userService.logout().catch(() => console.log('logout failed'));
  }

  onSubmit() {
    if (!this.restore) {
      this.login();
    } else {
      this.onRestore();
    }
  }

  login() {
    this.userService.login(this.usrName, this.password).then(user => {
      this.userService.logInEvent(user);
      this.userService.setToLocalStorage(user);
      this.router.navigate(['/']).catch();
    });
  }

  restoreToggle() {
    this.restore = !this.restore;
  }

  onRestore() {
    if (this.email != null) {
      Backendless.UserService.restorePassword(this.email).then(() => this.router.navigate(['/']));
      this.restore = false;
    }
  }
}
