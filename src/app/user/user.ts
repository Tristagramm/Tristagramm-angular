
export interface User extends Backendless.User {
  objectId?: string;
  name: string;
  email: string;
  country: string;
  sex: boolean;
  avatar?: string;
  age: number;
  geo: Backendless.GeoPoint;
  track: boolean;
}
