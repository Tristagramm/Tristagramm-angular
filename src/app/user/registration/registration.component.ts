import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: RegistrationComponent,
    multi: true
  },
    {
      provide: NG_VALIDATORS,
      useExisting: RegistrationComponent,
      multi: true
    }
  ]
})
export class RegistrationComponent implements OnInit {
  @ViewChild('avatar') fileInput;
  name: string;
  age: number;
  email: string;
  password: string;
  password2: string;
  country: string;
  sex: boolean;
  avatar: File;

  form: FormGroup;

  submitTouched: boolean;

  constructor(private userService: UserService, private router: Router) {
    this.userService.logInEvent(null);
  }

  ngOnInit() {
    this.submitTouched = false;

    // const passPattern = '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$';
    const mailPattern = '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)' +
      '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)' +
      '+[a-zA-Z]{2,}))$';

    this.form = new FormGroup({
      'name': new FormControl(this.name, [Validators.required, Validators.minLength(6)]),
      'age': new FormControl(this.age, [Validators.required, Validators.min(6)]),
      'email': new FormControl(this.email, [Validators.required, Validators.pattern(mailPattern)]),
      'password': new FormControl(this.password, [Validators.minLength(6)]),
      'country': new FormControl(this.country, [Validators.required])
    });
  }

  get nameC() {
    return this.form.get('name');
  }

  get ageC() {
    return this.form.get('age');
  }

  get emailC() {
    return this.form.get('email');
  }

  get countryC() {
    return this.form.get('country');
  }

  get passwordC() {
    return this.form.get('password');
  }

  onSubmit() {
    this.submitTouched = true;
    console.log(this.form.valid);
    if (this.form.valid && this.password === this.password2) {
      this.userService.registrate(this.name, this.age, this.email, this.password, this.sex, this.country, this.avatar)
        .then(e => {
          this.userService.successRegistration(e);
          this.router.navigate(['/']);
        }).catch(e => console.log('err: ' + e));
    }
  }

  fileChanged() {
    this.avatar = this.fileInput.nativeElement.files[0];
    console.log(this.avatar.name);
  }

}
