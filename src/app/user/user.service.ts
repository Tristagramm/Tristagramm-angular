import {EventEmitter, Injectable} from '@angular/core';
import {User} from './user';
import {Http} from '@angular/http';
import {HttpService} from '../global/http.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {BuiltinTypeName} from '@angular/compiler/src/output/output_ast';
import Backendless from 'backendless'
import {USER_STORAGE} from '../routes/route';
import {DataProvider} from '../global/data-provider';

@Injectable()
export class UserService extends DataProvider {

  public static currentUser: User;
  public static usersModel = 'Users';
  loginObserve = new Subject<User | null>();
  userLogged$ = this.loginObserve.asObservable();
  avatar: File;


  constructor() {
    super();
  }


  registrate(name: string, age: number, email: string, password: string, sex: boolean, country: string, avatar?: File) {
    this.avatar = avatar;
    const user = new Backendless.User();
    user['name'] = name;
    user['age'] = age;
    user['email'] = email;
    user['password'] = password;
    user['sex'] = sex;
    user['country'] = country;
    user['avatar'] = avatar.name;
    return Backendless.UserService.register(user);
  }

  currentUser() {
    return <Promise<User>>Backendless.UserService.getCurrentUser()
  }

  successRegistration(user: Backendless.User) {
    if (this.avatar != null && user['avatar'] != null) {
      this.uploadFile(this.avatar, user['name'])
    }
  }

  uploadFile(file: File, userName: string) {
    return Backendless.Files.upload(file, 'user/' + userName, true);
  }

  login(name: string, password: string) {
    console.log(name + ', ' + password)
    return Backendless.UserService.login(name, password, true)
  }

  logout() {
    this.logInEvent(null)
    localStorage.removeItem('user');
    localStorage.clear();
    return Backendless.UserService.logout()
  }

  logInEvent(user: Backendless.User) {
    UserService.currentUser = <User>user;
    this.loginObserve.next(<User>user);
  }

  setToLocalStorage(usr: Backendless.User) {
    this.setToStorage(localStorage, usr);
  }

  getFromLocalStorage(): User {
    return this.getFromStorage(localStorage);
  }

  private getFromStorage(storage: Storage) {
    const retrievedUserJSON = storage.getItem('user');
    try {
      return JSON.parse(retrievedUserJSON);
    } catch (e) {
      return null;
    }
  }

  private setToStorage(storage: Storage, usr: Backendless.User) {
    storage.setItem('user', JSON.stringify(usr));
  }

  getAvatarPath(user: User): string {
    return USER_STORAGE + user.name + '/' + user.avatar;
  }

  getUserByID(objectId) {
    return Backendless.Data.of(UserService.usersModel).findFirst()
  }

  getUserByName(name: string): Promise<User[]> {
    const qu = 'name = ' + this.quot(name);
    console.log(qu)
    return Backendless.Data.of(UserService.usersModel)
      .find(this.q(qu));
  }

}
