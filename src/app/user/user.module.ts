import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {ProfileComponent} from '../friends/profile/profile.component';
import {RegistrationComponent} from './registration/registration.component';
import {SexPipe} from '../friends/profile/sex.pipe';
import {PhotoModule} from '../photo/photo.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SubscribersComponent } from '../friends/subscribers/subscribers.component';

const appRoutes: Routes = [
  {path: 'registration', component: RegistrationComponent},
  {path: 'login', component: LoginComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
    PhotoModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    RegistrationComponent,
    LoginComponent
  ]
})
export class UserModule { }
