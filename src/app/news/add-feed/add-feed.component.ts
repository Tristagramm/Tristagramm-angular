import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../user/user';
import {FeedService} from '../../feed/feed.service';
import {FeedClass} from '../../feed/feed-class';
import {UserService} from '../../user/user.service';
import {Router} from '@angular/router';
import {Feed} from '../../feed/feed';

@Component({
  selector: 'app-add-feed',
  templateUrl: './add-feed.component.html',
  styleUrls: ['./add-feed.component.css']
})
export class AddFeedComponent implements OnInit {
  @Input() user?: User;
  @ViewChild('fileInput') fileInput;
  @Output() newFeedChange = new EventEmitter();

  text?: string;

  constructor(private feedService: FeedService, private userService: UserService, private router: Router) {
    // this.user = this.userService.getFromLocalStorage()
  }

  ngOnInit() {

  }

  onSubmit() {
    const file = this.fileInput.nativeElement.files[0];
    const feed = new FeedClass(this.user.objectId, this.user, this.text);
    if (file != null) {
      feed.photo = file.name;
    }
    this.feedService.createFeed(feed).then(() => {
      this.uploadFile(file)
        .then(() => this.router.navigate(['/'])).then(() => this.newFeed(feed));
    });
  }

  uploadFile(file: File) {
    if (file != null) {
      return Backendless.Files.upload(file, 'user/' + this.user.name);
    } else {
      return Promise.resolve(undefined);
    }
  }

  newFeed(feed) {
    this.newFeedChange.emit(feed);
  }

}
