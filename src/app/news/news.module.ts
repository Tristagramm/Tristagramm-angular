import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewsComponent} from './news.component';
import {RouterModule, Routes} from '@angular/router';
import { AddFeedComponent } from './add-feed/add-feed.component';
import {FormsModule} from '@angular/forms';
import {FeedModule} from '../feed/feed.module';
const appRoutes: Routes = [
  {path: '', component: NewsComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    FeedModule
  ],
  declarations: [
    NewsComponent,
    AddFeedComponent
  ],
})
export class NewsModule {
}
