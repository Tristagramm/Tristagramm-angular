import {Component, OnInit} from '@angular/core';
import {Feed} from '../feed/feed';
import {FeedService} from '../feed/feed.service';
import {User} from '../user/user';
import {UserService} from '../user/user.service';
import {Friend} from '../friends/friend';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [FeedService]
})
export class NewsComponent implements OnInit {

  feeds?: Feed[];
  user: User;

  constructor(private newsService: FeedService, private userService: UserService) {
    this.user = this.userService.getFromLocalStorage();
  }

  ngOnInit() {
    this.newsService.getNewsOf(this.user).then(e => this.feeds = e);
  }

  append(feed: Feed) {
    this.feeds.unshift(feed);
  }

  removeFromList(feed: Feed) {
    const idx = this.feeds.indexOf(feed);
    this.feeds.splice(idx, 1);
  }

}

