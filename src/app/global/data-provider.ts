export class DataProvider {

  protected q(query: string | null) {
    const dataQuery = Backendless.DataQueryBuilder.create();
    return dataQuery.setWhereClause(query);
  }

  protected quot(val: string) {
    return '\'' + val + '\'';
  }

}
