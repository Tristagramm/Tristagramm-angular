import { Injectable } from '@angular/core';

@Injectable()
export class GeolocationService {
  latitude: number;
  longitude: number;
  constructor() { }

}
