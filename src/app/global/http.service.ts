import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, RequestOptions, Response, URLSearchParams} from '@angular/http';

@Injectable()
export class HttpService {

  protected test: boolean;


  constructor(protected http: Http) {
    this.test = true;
  }

  protected setParams(params: { k: string, v: any }[]): RequestOptions {
    const cpParams = new URLSearchParams();
    params.forEach((val) => (val.k != null && val.v != null) ? cpParams.append(val.k, val.v) : null);
    return new RequestOptions({params: cpParams});
  }

  protected extractData(res: Response) {
    const body = res.json();
    return body;
  }

  protected handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }
}
