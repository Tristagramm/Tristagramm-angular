export interface Page {
  text: string;
  ref: string;
  classes: string[];
  dropdowns?: Page[];
  restriction?: Restriction;
}

export enum Restriction {
  All = 1,
  Guest,
  User,
  Admin
}
