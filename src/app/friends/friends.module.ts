import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FriendsComponent} from './friends.component';
import {SubscribersComponent} from './subscribers/subscribers.component';
import {PhotoModule} from '../photo/photo.module';
import { UserSearchComponent } from './user-search/user-search.component';
import {ProfileComponent} from './profile/profile.component';
import {SexPipe} from './profile/sex.pipe';
import {FeedModule} from '../feed/feed.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {path: 'subscribes', component: FriendsComponent, pathMatch: 'full'},
  {path: 'subscribers', component: SubscribersComponent, pathMatch: 'full'},
  {path: 'search/:search', component: UserSearchComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'profile/:name', component: ProfileComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
    PhotoModule,
    FeedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    FriendsComponent,
    SubscribersComponent,
    ProfileComponent,
    UserSearchComponent,
    SexPipe
  ]
})
export class FriendsModule {
}
