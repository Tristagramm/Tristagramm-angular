import {Component, OnInit} from '@angular/core';
import {UserService} from '../user/user.service';
import {User} from '../user/user';
import {FriendService} from './friend.service';
import {Friend} from './friend';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css'],
  providers: [UserService, FriendService]
})
export class FriendsComponent implements OnInit {

  user: User;
  friends: Friend[];

  constructor(private userService: UserService, private friendService: FriendService) {
    this.userService.userLogged$.subscribe(u => this.afterUserInit(u))
  }

  ngOnInit() {
    this.user = this.userService.getFromLocalStorage();
    this.afterUserInit(this.user);
  }

  afterUserInit(user: User) {
    this.user = user;
    this.friendService.getSubscribedOn(user).then(fr => this.afterFriendsInit(fr));
  }

  afterFriendsInit(friends: Friend[]) {
    this.friends = friends;
  }

  avatar(user: User) {
    return this.userService.getAvatarPath(user)
  }

  removeFriend(friend: Friend) {
    this.friendService.declineFriendship(friend).then(() => this.removeFromList(friend));
  }

  removeFromList(friend: Friend) {
    const idx = this.friends.indexOf(friend);
    this.friends.splice(idx, 1);
  }

}
