import {Component, OnInit} from '@angular/core';
import {FriendService} from '../friend.service';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['../friends.component.css'],
  providers: [FriendService, UserService]
})
export class UserSearchComponent implements OnInit {

  user: User;
  found: User[];

  constructor(private friendService: FriendService, private userService: UserService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.user = this.userService.getFromLocalStorage();
    this.activatedRoute.paramMap.subscribe(v => this.afterPathCheck(v.get('search')));
  }

  afterPathCheck(query: string) {
    this.friendService.findUsers(query, this.user)
      .then(f => {this.found = f; console.log(f)})
      .catch(err => console.log(err));
  }


  avatar(user: User) {
    return this.userService.getAvatarPath(user)
  }

}
