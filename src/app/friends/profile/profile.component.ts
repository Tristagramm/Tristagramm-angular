import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Feed} from '../../feed/feed';
import {FeedService} from '../../feed/feed.service';
import {FriendService} from '../friend.service';
import {Friend} from '../friend';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [FeedService, UserService]
})
export class ProfileComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  user?: User;
  avatarPath?: string;
  updateAvatar = false;
  avatar: File;
  feeds?: Feed[];
  isCurrentUser = false;
  friendship: Friend[];

  constructor(private userService: UserService, private friendService: FriendService,
              private feedService: FeedService, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(map => this.checkParams(map));
  }

  checkParams(map: ParamMap) {
    const userName = map.get('name');
    if (userName != null) {
      this.userService.getUserByName(userName).then(u => this.afterUserChecked(u[0]));
    } else {
      this.isCurrentUser = true;
      this.userService.currentUser().then(u => this.afterUserChecked(u));
    }
  }

  afterUserChecked(u: User) {
    console.log(u);
    this.user = u;
    this.avatarPath = this.userService.getAvatarPath(u);
    this.feedService.getUsersNews(u).then(f => this.feeds = f);
    this.canISubscribeOn(u);
  }

  canISubscribeOn(user: User) {
    if (!this.isCurrentUser) {
      this.friendService.isSubscribedOn(UserService.currentUser, user).then(f => this.friendship = <Friend[]>f);
    }
  }

  onSubscribe() {
    this.friendService.requestFriendship().then(fr => {
      this.friendship = [fr];
      Backendless.Data.of('Friends').setRelation(fr, 'friend1', [UserService.currentUser]);
      Backendless.Data.of('Friends').setRelation(fr, 'friend2', [this.user]);
    });
  }

  onDecline() {
    this.friendService.declineFriendship(this.friendship.pop()).then(() => {
      this.friendship = [];
    });
  }

  updateClick() {
    this.updateAvatar = true;
  }

  fileChanged() {
    this.avatar = this.fileInput.nativeElement.files[0];
    if (this.avatar != null) {
      this.user.avatar = this.avatar.name;
      this.userService.uploadFile(this.avatar, this.user.name).then(() => {
        Backendless.UserService.update(<Backendless.User>this.user).then(() => {
          console.log('Success');
          this.userService.logInEvent(this.user);
          this.ngOnInit();
        }).catch(e => console.log(e));
      }).catch(e => console.log(e));
    }
  }

  userSubmit() {
    Backendless.UserService.update(this.user).then(() => {
      console.log('Success');
      this.userService.logInEvent(this.user);
      this.ngOnInit();
    })
  }

}
