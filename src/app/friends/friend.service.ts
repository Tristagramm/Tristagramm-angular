import {Injectable} from '@angular/core';
import {User} from '../user/user';
import {DataProvider} from '../global/data-provider';
import {Friend} from './friend';

@Injectable()
export class FriendService extends DataProvider {

  user: User;
  public static friendsModel = 'Friends';

  constructor() {
    super();
  }

  requestFriendship(): Promise<Friend> {
    const friend = <Friend>{status: false};
    const model = Backendless.Data.of(FriendService.friendsModel);
    return model.save(friend);
  }

  getSubscribedOn(user: User): Promise<Friend[]> {
    const quName = '\'' + user.name + '\'';
    const query = this.q('friend1.name = ' + quName)
      .setRelated(['friend2']);
    return Backendless.Data.of(FriendService.friendsModel).find(query);
  }

  getSubscribers(user: User): Promise<Friend[]> {
    const quName = '\'' + user.name + '\'';
    const query = this.q('friend2.name = ' + quName)
      .setRelated(['friend1']);
    return Backendless.Data.of(FriendService.friendsModel).find(query);
  }

  getSubscribersCount(user: User) {
    const quName = '\'' + user.name + '\'';
    const query = this.q('friend2.name = ' + quName)
      .setRelated(['friend1']);
    return Backendless.Data.of(FriendService.friendsModel).getObjectCount(query);
  }

  getSubscribedOnCount(user: User) {
    const quName = '\'' + user.name + '\'';
    const query = this.q('friend1.name = ' + quName)
      .setRelated(['friend2']);
    return Backendless.Data.of(FriendService.friendsModel).getObjectCount(query);
  }

  declineFriendship(friend: Friend) {
    return Backendless.Data.of(FriendService.friendsModel).remove(friend);
  }

  findUsers(name: string, user: User): Promise<User[]> {
    const query = this.q('name != ' + this.quot(user.name) + ' and name like ' + this.quot(name));
    console.log(query.getWhereClause());
    return Backendless.Data.of('Users').find(query)
  }

  isSubscribedOn(curUser, user: User) {
    const query = this.q('friend1.name = ' + this.quot(curUser.name)
      + ' and friend2.name = ' + this.quot(user.name));
    return Backendless.Data.of(FriendService.friendsModel).find(query);
  }

}
