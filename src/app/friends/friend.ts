import {User} from '../user/user';

export interface Friend {
  friend1?: User;
  friend2?: User;
  status?: boolean;
  ownerId?: string;
  objectId: string;
  updated?: string;

}
